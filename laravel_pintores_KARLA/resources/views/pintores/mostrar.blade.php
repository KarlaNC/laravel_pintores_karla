@extends('layouts.master')

@section('titulo')
	Mostrar
@endsection

@section('contenido')
	<div class="row">
			<h3 class="display-4">{{ $pintorSeleccionado->nombre }}</h4>
			<h4 class="display-5"> País: {{ $pintorSeleccionado->pais }}</h4>
			<h4 class="display-4"> Cuadros: {{ $pintorSeleccionado->pais }}</h4>
			
			@foreach ($pintorSeleccionado->cuadros as $cuadro)
			<div class="col-sm-14">
					<p>{{ $cuadro->nombre }}</p>
					<img src="{{ asset('assets/cuadros/')}}/{{ $cuadro->imagen }}" style="height:170px" class="border border-light"/>
		</div>
			@endforeach
			
	</div>

@endsection