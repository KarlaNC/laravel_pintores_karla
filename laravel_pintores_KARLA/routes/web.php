<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@getInicio');

Route::get('pintores', 'PintoresController@getTodos');

Route::get('/pintores/mostrar/{id}', 'PintoresController@getVer')->where('id','[0-9]+');

Route::get('cuadros/crear', 'PintoresController@getCrear');
Route::post('cuadros/crear', 'PintoresController@postCrear');
