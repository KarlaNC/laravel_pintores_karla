<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pintor extends Model
{
   	protected $table = 'pintores';

   	public function cuadros(){
		return $this->hasMany("App\Cuadro");
	}
}
