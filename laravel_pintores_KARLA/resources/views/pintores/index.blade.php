@extends('layouts.master')

@section('titulo')
	Index
@endsection

@section('contenido')

	@if (session('mensaje'))
		<div class="alert alert-success" role="alert">
		  {{ session('mensaje') }}
		</div>
	@endif

	<div class="row">
		@foreach( $arrayPintores as $pintor )
			<div class="col-xs-12 col-sm-6 col-md-2 ">
				<table border = 1px>
					<tr>
			            <th>Nombre</th>
						<th>Pais</th>
						<th>Cuadros</th>
			        </tr>
			        <tr>
			        	<td> <a href=" {{ url('/pintores/mostrar' ) }}/{{$pintor->id }}"> {{$pintor->nombre}} </a></td>
						<td>{{$pintor->pais}}</td>
						<td>{{$pintor->cuadros->count()}}</td>
			       </tr>
				</table>
		@endforeach
	</div>

@endsection