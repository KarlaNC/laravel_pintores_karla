<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pintor;
use App\Cuadro;
use Illuminate\Support\Facades\Storage;

class PintoresController extends Controller
{
    public function getTodos(){
    	$pintores = Pintor::all();
		return view('pintores.index', array('arrayPintores' => $pintores));
	}

	public function getVer($id){ //Con id-nombre
    	$pintorID = Pintor::findOrFail($id);
		return view('pintores.mostrar',  array('pintorSeleccionado' => $pintorID));
	}

	public function postCrear(Request $request){

		$cuadro = new Cuadro();
		$pintor = Pintor::all()->where('nombre',$request->nombre_pintor);
		$cuadro->pintor_id = $pintor->find('nombre',$request->nombre_pintor); 
		$cuadro->nombre =  $request->nombre;
		$cuadro->imagen =  $request->imagen->store( "",'cuadros'); // 

		try {
			$cuadro->save();
			return redirect("pintores")->with("mensaje", "Cuadro registrado con exito");
		} catch (Exception $ex) { //\Illuminate\Database\QueryException
			return redirect("pintores")->with("mensaje", "Fallo registrar el cuadro");
		}
	}

	public function getCrear(){	
		$pintores = Pintor::all();
		return view('pintores.crear',  array('arrayPintores' => $pintores));

	}
}
