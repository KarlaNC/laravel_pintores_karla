@extends('layouts.master')

@section('titulo')
    Crear
@endsection

@section('contenido')
<div class="row">
  <div class="offset-md-3 col-md-6">
    <div class="card">
      <div class="card-header text-center">
       Subir cuadro
      </div>
    <div class="card-body" style="padding:30px">
  {{-- TODO: Abrir el formulario e indicar el método POST --}}
    <form action="{{ url('cuadros/crear') }}" method="post" enctype="multipart/form-data"> {{--url('mascotas/crear')--}}
      {{ csrf_field() }}
      {{-- TODO: Protección contra CSRF --}}
        <div class="form-group">
          <label for="nombre">Nombre Cuadro</label>
          <input type="text" name="nombre" id="nombre" class="form-control">
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la especie --}}
        <label for="nombre_pintor">Nombre del cuadro</label>
          <select name="nombre_pintor" id="nombre_pintor" >
            @foreach ($arrayPintores as $pintor)
              <option value="{{ $pintor->id }}"> {{ $pintor->nombre }}</option>
             @endforeach
          </select>
          {{-- <input type="text" name="especie" id="especie" class="form-control"> --}}
        </div>

        <div class="form-group">
        {{-- TODO: Completa el input para la imagen --}}
        <label for="imagen">Imagen</label>
        <input name="imagen" type="file" class="btn btn-outline-dark" />
        </div>
        <div class="form-group text-center">
          <button type="submit" class="btn btn-outline-dark" >Añadir cuadro</button>
        </div>
    </form>
    {{-- TODO: Cerrar formulario --}}
    </div>
    </div>
  </div>
</div>
@endsection